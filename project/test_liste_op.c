#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 10

void liste_op(int op, char* tab, int i);

int main()
{
    char* tab = NULL;
    int op, size;
    printf("\nNombre d'opérateurs : ");
    scanf("%d", &size);
    
    tab = calloc(size, sizeof(*tab));
    
    for(int i = 0; i<size; i++)
    {
        printf("\nOpérateur (0-->'+', 1-->'-', 2-->'*', 3-->'/' : ");
        scanf("%d", &op);
        liste_op(op, tab,i);
    }
    
    for(int j = 0; j<size; j++)
    {
        printf("\nOpérateur : %c ", tab[j]);
    }
    
    return 0;
}

void liste_op(int op, char* tab, int i)
{
    switch(op)
    {
        case 0:
            tab[i] = '+';
    break;
    
        case 1 :
            tab[i] = '-';
    break;
    
        case 2 :
            tab[i] = '*';
    break;
            
    
        case 3 :
            tab[i] = '/';
    break;
    
        default :
            break;
    }
    
}
