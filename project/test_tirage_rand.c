#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define N 10 //nombre de chiffres tirés au hasard

void tirage_rand(int plaques[], int* cible, int tirage[]);


int main (void)
{
    srand(time(NULL));
    
    int cible;//valeur à atteindre
    int tirage[N];//tableau des valeurs tirées
    int plaques[24] = {1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10,25,50,75,100};
    tirage_rand(plaques, &cible, tirage);
    
    int i;
    printf("Plaques tirées : ");
    for(i = 0; i < N; i++) //affichage des valeurs tirées
    {
        printf("%d      ", tirage[i]);
    }
    printf("\nLa valeur cible est %d\n", cible);
     
    return 0;
    
}


void tirage_rand(int plaques[], int* cible, int tirage[])
{
    int i,j,save,range;
    *cible = rand() % 100000; //tire un nombre au hasard entre 0 et 100000
    
    for(j = 0; j < N; j++)
    {
        range = 23-j;
        i = rand()%range; //tire un nombre au hasard entre 0 et 23 à la 1ère itération puis entre 0 et 22 et ainsi de suite
        //printf("\ni = %d", i);
        tirage[j] = plaques[i];
        save = plaques[23-j];
        plaques[23-j] = plaques[i];
        plaques[i] = save;
        
        int t;
        for(t = 0; t < 24; t++)// affichage des plaques ré-arrangées
        {
            printf(" %d      ", plaques[t]);
        }
        printf("\n");
    }

}
