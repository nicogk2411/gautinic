#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int op_possible(int* tirage, int nb, int i, int j, int op);

int main(void)
{
    
    int tirage[6] = {0,1,2,3,4,5};
    int a, b , op;
    
    printf("\n Voici les éléments du tableau : ");
    
    for(int i = 0; i < 6; i++)
    {
        printf("\t %d", tirage[i]);
    }
    
    printf("\n Choisir deux nombres : ");
    printf("\n a :");
    scanf("%d", &a);
    printf("\n b :");
    scanf("%d", &b);
    printf("\n Choisir un opérateur ([0 : '-'], [1 : '+'], [2 : '/'], [3 : '*']");
    scanf("%d", &op);
    
    
    if(op_possible(tirage, 6, a, b, op)==1)
    {
        printf("\n L'opération est autorisée");
    }
    else
    {
        printf("Calcul interdit");
    }
    
    return 0;
}

int op_possible(int* tirage, int nb, int i, int j, int op)// vérifie que l'opération demandée est possible
{
    int  a = tirage[i];
    int  b = tirage[j];
    
    switch(op)
    {
        case 0 :
            if(a>b && b !=0) //vérification soustraction
            {
                tirage[i] = a - b;
                return 1;
            }
            else
                return 0;
            break;
            
        case 1 :
            tirage[i] = a + b;
            return 1;
            break;
            
        case 2 :
            if(a%b == 0 && b!= 1 && b != 0)//vérification de la division
            {
                tirage[i] = a/b;
                return 1;
            }
            else
                return 0;
            break;
            
        case 3 :
            if(b != 1 &&  b!= 0)// vérification multiplication
            {
                tirage[i] = a*b;
                return 1;
            }
            else
                return 0;
            break;
            
        default :
            return 0;
            break;
    }
}
