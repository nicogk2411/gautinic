#include "Projet_info.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 10 //nombre de chiffres tirés au hasard

int resolution(int* tirage, int nb, int cible, char** operations);
void tirage_rand(int plaques[], int* cible, int tirage[]);
int op_possible(int* tirage, int nb, int i, int j, int op);
void liste_op(int op, char* tab_op, int i);

int main (void)
{
    srand(time(NULL));
    
    int cible;//valeur à atteindre
    int tirage[N];//tableau des valeurs tirées
    char *operations;
    int result;
    int plaques[24] = {1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10,25,50,75,100};
    tirage_rand(plaques, &cible, tirage);
    /*
     int i;
     for(i = 0; i < N; i++) //affichage des valeurs tirées
    {
        printf("%d      ", tirage[i]);
    }
    printf("La valeur cible est %d\n", cible);
     */
    
    
    printf("\nbalise1");
    result = resolution(tirage, N, cible, &operations);
    printf("\nbalise7");
    printf("\n %d", result);
    return 0;
}

void tirage_rand(int plaques[], int* cible, int tirage[])
{
    int i,j,save,range;
    *cible = rand() % 100000; //tire un nombre au hasard entre 0 et 100000
    
    for(j = 0; j < N; j++)
    {
        range = 23-j;
        i = rand()%range; //tire un nombre au hasard entre 0 et 23 à la 1ère itération puis entre 0 et 22 et ainsi de suite
        //printf("\ni = %d", i);
        tirage[j] = plaques[i];
        save = plaques[23-j];
        plaques[23-j] = plaques[i];
        plaques[i] = save;
        
        /*int t;
        for(t = 0; t < 24; t++)// affichage des plaques ré-arrangées
        {
            printf("%d      ", plaques[t]);
        }
        printf("\n");*/
    }

}

int resolution(int* tirage, int nb, int cible, char** operations)
{
    
    int op = 0;
    int i = 0;
    int save, j;
    int result = 0;
    char* tab_op = NULL;
    
    srand(time(NULL));
    
    for (i = 0; i < nb; i++)
    {
        int range = 0;
        op = rand()%4;// choisit une valeur entre 0 et 3. 0 --> '+', 1 --> '-', 2 --> '*", 3 --> '/'
        switch(op)
        {
            case 0 : //addition
                range = nb-i;
                j = rand()%range;
                if(op_possible(tirage, N, i, j, op) == 1)
                {
                    save = tirage[nb-i];
                    tirage[nb-i] = tirage[j];
                    tirage[j] = save;
                    tab = calloc(i+1, sizeof(*tab));
                    liste_op(op, tab_op, i);
                }
                else
                {
                    ;
                }
                break;
            case 1 : //soustraction
                range = nb-i;
                j = rand() % range;
                if(op_possible(tirage, N, i, j, op) == 1)
                {
                    save = tirage[nb-i];
                    tirage[nb-i] = tirage[j];
                    tirage[j] = save;
                    tab = calloc(i+1, sizeof(*tab));
                    liste_op(op, tab_op, i);
                }
                else
                {
                    ;
                }
                break;
            case 2 : //multiplication
                range = nb-i;
                j = rand() % range;
                if(op_possible(tirage, N, i, j, op) == 1)
                {
                    save = tirage[nb-i];
                    tirage[nb-i] = tirage[j];
                    tirage[j] = save;
                    tab = calloc(i+1, sizeof(*tab));
                    liste_op(op, tab_op, i);
                }
                else
                {
                    ;
                }
                break;
            case 3 : //division
                range = nb-i;
                j = rand() % range;
                if(op_possible(tirage, N, i, j, op) == 1)
                {
                    save = tirage[nb-i];
                    tirage[nb-i] = tirage[j];
                    tirage[j] = save;
                    tab = calloc(i+1, sizeof(*tab));
                    liste_op(op, tab_op, i);
                }
                else
                {
                    ;
                }
                break;
            default :
                ;
                break;
        }
    }
    printf("\nbalise6");
    
    return result;
}

int op_possible(int* tirage, int nb, int i, int j, int op)// vérifie que l'opération demandée est possible
{
    int  a = tirage[i];
    int  b = tirage[j];
    
    switch(op)
    {
        case 0 :
            if(a>b && b !=0) //vérification soustraction
            {
                tirage[i] = a - b;
                return 1;
            }
            else
                return 0;
            break;
            
        case 1 :
            tirage[i] = a + b;
            return 1;
            break;
            
        case 2 :
            if(a%b == 0 && b!= 1 && b != 0)//vérification de la division
            {
                tirage[i] = a/b;
                return 1;
            }
            else
                return 0;
            break;
            
        case 3 :
            if(b != 1 &&  b!= 0)// vérification multiplication
            {
                tirage[i] = a*b;
                return 1;
            }
            else
                return 0;
            break;
            
        default :
            return 0;
            break;
    }
}


void liste_op(int op, char* tab_op, int i)//remplit un tableau contenant les différentes opérations réalisées
{
    switch(op)
    {
        case 0:
            tab_op[i] = '+';
    break;
    
        case 1 :
            tab_op[i] = '-';
    break;
    
        case 2 :
            tab_op[i] = '*';
    break;
            
    
        case 3 :
            tab_op[i] = '/';
    break;
    
        default :
            break;
    }
    
}
